package sample;

import java.util.Random;

class Point{
  //Point Attributes:
  int x = 0;
  int y = 0;

  //Point Constructor:
//default
  public Point(){
  }
  public Point(int x,int y){
    this.x = x;
    this.y = y;
  }

  //Point Functions:
  public void setX(int newX){
    this.x = newX;
  }
  public void setY(int newY){
    this.y = newY;
  }
  public void setXY(int newX, int newY){
    this.x = newX;
    this.y = newY;
  }
  @Override
  public String toString(){
    return "(" + x + "/" + y + ")";
  }
}


class Timer{
  //Timer Attributes
  long tick = 1;
  long max = 0;
  long value = 0;

  //Timer Constructor:
  public Timer(int tick, int max){
    this.tick = tick;
    this.max = max;
  }

  //Timer Functions:
  public boolean round(){
    if (value >= max){
      System.out.println("Time ended!");
      return false;
    }else{
      value += tick;
      return true;
    }
  }
  @Override
  public String toString(){
    return "" + value;
  }
}
