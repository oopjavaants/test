package sample;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
public class FoodSource{
  //FoodSource Attributes:
  int amount = 1;
  double decay = 0.0;
  Point location;

  //FoodSource Constructor:
  public FoodSource(Point location){
    this.location = location;
  }
  public FoodSource(Point location, int amount){
    this.location = location;
    this.amount = amount;
  }

  //FoodSource Functions:
  public int getX(){
    return this.location.x;
  }
  public int getY(){
    return this.location.y;
  }

  public boolean decrement(int amount){
    this.amount -= amount;
    if ((this.amount <= 0)) {
      return true;
    } else {
      return false;
    }
  }

  public Point getLocation() {
    return location;
  }

  //public void spoil(){
//        this.amount = this.amount * this.decay;
//    }
}
