package sample;

  import javafx.animation.RotateTransition;
  import javafx.animation.SequentialTransition;
  import javafx.event.ActionEvent;
  import javafx.event.EventHandler;
  import javafx.geometry.Insets;
  import javafx.scene.Scene;
  import javafx.scene.canvas.Canvas;
  import javafx.scene.control.Button;
  import javafx.scene.control.Label;
  import javafx.scene.control.TextField;
  import javafx.scene.layout.HBox;
  import javafx.scene.layout.VBox;
  import javafx.stage.Stage;
  import sample.service.FormService;
  import sample.service.UI_CreatorService;
  import sample.service.UI_CreatorServiceImpl;
  import sample.service.FormServiceImpl;

  import java.util.Scanner;


public class Main extends AbstractConfig {

  private UI_CreatorService uiCreatorService;

  private final FormService formService = new FormServiceImpl();


  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    this.primaryStage = primaryStage;
    this.primaryStage.setTitle("Ant Colony simulator");

    //Creating a VBox container
    VBox box = new VBox();
    //Defining the Width of foodField text field
    Label label1 = new Label("Width of foodField (100-1000):");
    label1.setPrefWidth(250.0);
    //label1.setPadding( 20.00 );
    TextField widthField = new TextField();
    widthField.getText();
    HBox hb1 = new HBox();
    hb1.getChildren().addAll(label1, widthField);
    hb1.setSpacing(10);
    hb1.setPadding(new Insets(10, 10, 0, 10));

    Label label2 = new Label("Height of foodField (100-490):");
    label2.setPrefWidth(250.0);
    TextField heightField = new TextField();
    heightField.getText();
    HBox hb2 = new HBox();
    hb2.getChildren().addAll(label2, heightField);
    hb2.setSpacing(10);
    hb2.setPadding(new Insets(10, 10, 0, 10));

    Label label4 = new Label("X-coordinate location of hill (10-980):");
    label4.setPrefWidth(250.0);
    TextField hillFieldX = new TextField();
    HBox hb4 = new HBox();
    hb4.getChildren().addAll(label4, hillFieldX);
    hb4.setSpacing(10);
    hb4.setPadding(new Insets(10, 10, 0, 10));

    Label label5 = new Label("Y-coordinate location of hill (10-680):");
    label5.setPrefWidth(250.0);
    TextField hillFieldY = new TextField();
    HBox hb5 = new HBox();
    hb5.getChildren().addAll(label5, hillFieldY);
    hb5.setSpacing(10);
    hb5.setPadding(new Insets(10, 10, 0, 10));

    Label label3 = new Label("Amount of FoodSource (min.1):");
    label3.setPrefWidth(250.0);
    TextField foodField = new TextField();
    HBox hb3 = new HBox();
    hb3.getChildren().addAll(label3, foodField);
    hb3.setSpacing(10);
    hb3.setPadding(new Insets(10, 10, 0, 10));

    //   HBox.setHgrow(textField1, Priority.ALWAYS);
    //  textField1.setMaxWidth(Double.MAX_VALUE);


    Button button = new Button();
    button.setText("Draw");

//            Path path = new Path();
//            path.getElements().add(new MoveTo( 500,0 ));
//            path.getElements().add(new CubicCurveTo( 380,0,380, 120, 200, 120 ));
//         //   path.getElements().add(new LineTo(200,100));
//
////            ArcTo arc = new ArcTo();
////            arc.setX( 250.0 );
////            arc.setY( 250.0 );
////            arc.setRadiusX( 50.0 );
////            arc.setRadiusY( 50.0 );
////        path.getElements().add(arc);
////        path.getElements().add(new ClosePath());
//
////        Polyline polyline = new Polyline();
////        polyline.getPoints().addAll(new Double[]{
////                0.0, 0.0,
////                200.0, 100.0,
////                100.0, 200.0});
////        Circle circle = new Circle( 100);
////        Rectangle rect = new Rectangle( 100, 100 );
//        PathTransition transition = new PathTransition();
//        transition.setNode( button );
//        transition.setDuration(javafx.util.Duration.seconds(3));
//        transition.setPath( path );
//       // transition.setOrientation( PathTransition.OrientationType.ORTHOGONAL_TO_TANGENT );
//        transition.setCycleCount( PathTransition.INDEFINITE );
//        transition.play();

    //Defining the Clear button
    Button clear = new Button("Clear");

    HBox hb6 = new HBox();
    hb6.getChildren().addAll(button, clear);
    hb6.setSpacing(10);
    hb6.setPadding(new Insets(10, 10, 10, 10));
    HBox hbox = new HBox();
    box.getChildren().addAll(hb1, hb2, hb4, hb5, hb3, hb6, hbox);
    button.setOnAction(new EventHandler<ActionEvent>() {
      @Override
      public void handle(ActionEvent actionEvent) {
        Scanner sc = new Scanner(System.in);

        int foodSource = Integer.parseInt(foodField.getText());
        int widthF = Integer.parseInt(widthField.getText());
        int heightF = Integer.parseInt(heightField.getText());
        int antHillX = Integer.parseInt(hillFieldX.getText());
        int antHillY = Integer.parseInt(hillFieldY.getText());

        F1 = new Field(cmd, widthF, heightF, antHillX, antHillY, antCount, antSpeed, foodSource);

        uiCreatorService = new UI_CreatorServiceImpl(F1);
        if (formService.validateData(widthF, heightF, antHillX, antHillY, foodSource, widthField, heightField, hillFieldX, hillFieldY, foodField)) {
          Canvas sharpCanvas = uiCreatorService.createCanvasGrid(widthF, heightF, antHillX, antHillY, foodSource, true);
          box.getChildren().add(sharpCanvas);
//                        Rectangle antHill = new Rectangle(antHillX, antHillY, 20, 20 ); // Draw the AntHill
//                        System.out.println("X: " + antHill.getX());
//                        System.out.println("Y: " + antHill.getY());
//                        antHill.setFill(Color.DARKRED);
//                        hbox.getChildren().add(antHill);
//                        //foodList.size()
//                        for (int i = 0; i < foodList.size(); i++) {
//                            // Random rnd = new Random();
//                            int ah = foodList.get( i ).location.y;  //create random food source
//                            int aw = foodList.get( i ).location.x;
//                            System.out.println( "Ant" + i + " moved to: " + ah + "(ah) and " + aw + "(aw)" );
//                            // System.out.println("Ant" + i + " moved to: " + aw);
//                            Rectangle rectangle = new Rectangle(aw, ah, 10, 10);  // Draw the food source
//                            rectangle.setFill( Color.DARKGREEN );
//
//                            Circle circle = new Circle( 5 );
//                            circle.setFill( Color.GRAY );
//                            Path path = new Path();
//                            path.getElements().add( new MoveTo( antHillX, antHillY ) );
//                            path.getElements().add( new CubicCurveTo( 380, 0, 380, 120, ah, aw ) );
//                            path.getElements().add(new ClosePath());
//                            PathTransition transition = new PathTransition();
//                            transition.setNode( circle );
//                            transition.setDuration( javafx.util.Duration.seconds( 7 ) );
//                            transition.setPath( path );
//                            transition.play();
//                            hbox.getChildren().addAll( rectangle, circle );
//                        }
        } else {
          RotateTransition rt1 = new RotateTransition(javafx.util.Duration.millis(50), box);
          int byAngle = 15;
          rt1.setByAngle(byAngle);
          rt1.setCycleCount(2);
          rt1.setAutoReverse(true);
          RotateTransition rt2 = new RotateTransition(javafx.util.Duration.millis(50), box);
          rt2.setByAngle(-byAngle);
          rt2.setCycleCount(2);
          rt2.setAutoReverse(true);
          SequentialTransition st = new SequentialTransition(rt1, rt2);
          st.play();
        }
//                    widthField.setVisible( false );
//                    heightField.setVisible( false );
//                    hillFieldX.setVisible( false );
//                    hillFieldY.setVisible( false );
//                    foodField.setVisible( false );
//                    button.setVisible( false );
//                    clear.setVisible( false );
      }
    });

        //Setting an action for the Clear button
        clear.setOnAction(new EventHandler<ActionEvent>() {

          @Override
          public void handle(ActionEvent e) {
            box.getChildren().clear();
            try {
              start(primaryStage);
            } catch (Exception e1) {
              e1.printStackTrace();
            }
          }
        });

//        sharpCanvas.widthProperty().bind(primaryStage.widthProperty());
//        sharpCanvas.heightProperty().bind(primaryStage.heightProperty());

    primaryStage.setMinHeight(700);
    primaryStage.setMinWidth(1000);
    Scene scene = new Scene(box);
    //box.getStylesheets().add( "sample/style.css" );
    primaryStage.setResizable(true);
    // primaryStage.sizeToScene();
    primaryStage.setScene(scene);
    primaryStage.show();

  }


  //  /**
//   * Возвращает главную сцену.
//   * @return
//   */
  public Stage getPrimaryStage() {
    return primaryStage;
  }

  public UI_CreatorService getUiCreatorService() {
    return uiCreatorService;
  }

  public void setUiCreatorService(UI_CreatorService uiCreatorService) {
    this.uiCreatorService = uiCreatorService;
  }

  public FormService getFormService() {
    return formService;
  }
}
