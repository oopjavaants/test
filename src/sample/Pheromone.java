package sample;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
public class Pheromone {
  //Pheromone Attributes:
  double strength = 1;
  double decay = 0.5;
  Point location;

  //Pheromone Constructor:
  public Pheromone(Point position) {
    this.location = position;
  }

  public Pheromone(Point position, float strength) {
    this.location = position;
    this.strength = strength;
  }

  public Pheromone(Point position, float strength, float decay) {
    this.location = position;
    this.strength = strength;
    if (decay > 1) {
      this.decay = 1.0;
    } else {
      this.decay = decay;
    }
  }

  //Pheromone Functions:
  public int getX(){
    return this.location.x;
  }
  public int getY(){
    return this.location.y;
  }

  public boolean triggerDecay() {
    this.strength = this.strength * this.decay;
    return !(this.strength < 0.1);
  }

  public double getStrength(){
    return this.strength;
  }
}
