package sample.service;

import javafx.scene.control.TextField;

public class FormServiceImpl implements FormService{

  @Override
  public boolean validateData(int width, int height, int hillX, int hillY, int foodCount, TextField wf, TextField hf, TextField hfX, TextField hfY, TextField ff) {
    boolean isValid = true;
    if (100 > width || width > 1000) {
      wf.setStyle("-fx-border-color: red");
      isValid = false;
    }

    if (100 > height || height > 490) {
      hf.setStyle("-fx-border-color: red");
      isValid = false;
    }

    if (10 > hillX || hillX > (width - 20)) {
      hfX.setStyle("-fx-border-color: red");
      isValid = false;
    }

    if (10 > hillY || hillY > (height - 20)) {
      hfY.setStyle("-fx-border-color: red");
      isValid = false;
    }

    if (0 >= foodCount) {
      ff.setStyle("-fx-border-color: red");
      isValid = false;
    }
//        if (1 > antCount || antCount > 20) {
//            isValid = false;
//        }

    return isValid;
  }

}
