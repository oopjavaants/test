package sample.service;

import javafx.scene.canvas.Canvas;

public interface UI_CreatorService {
  Canvas createCanvasGrid(int width, int height, int hillX, int hillY, int fCount, boolean sharp);
}
