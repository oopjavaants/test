package sample.service;

import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import sample.*;

import java.util.List;

public class UI_CreatorServiceImpl extends AbstractConfig implements UI_CreatorService {

  public UI_CreatorServiceImpl(Field F1) {
    this.F1 = F1;
  }

  @Override
  public Canvas createCanvasGrid(int width, int height, int hillX, int hillY, int fCount, boolean sharp) {
    Canvas canvas = new Canvas( width, height );
    GraphicsContext gc = canvas.getGraphicsContext2D();
    gc.setLineWidth( 1.0 );
    for (int x = 0; x < width; x += 10) {
      double x1;
      if (sharp) {
        x1 = x + 0.5;
      } else {
        x1 = x;
      }
      gc.moveTo( x1, 0 );
      gc.lineTo( x1, height );
      gc.stroke();
    }

    for (int y = 0; y < height; y += 10) {
      double y1;
      if (sharp) {
        y1 = y + 0.5;
      } else {
        y1 = y;
      }
      gc.moveTo( 0, y1 );
      gc.lineTo( width, y1 );
      gc.stroke();
    }


    //AntList:
    antList = F1.getAntList();

    //FoodSourceList:
    foodList = F1.getFoodList();
    System.out.println( "\n" );
    for (int i = 0; i < foodList.size(); i++) {
      System.out.println( "FoodSource" + i + " is at: " + foodList.get( i ).getLocation() );
//         System.out.println("FoodSourceX" + i + " is at: " + foodList.get(i).getX());
//         System.out.println("FoodSourceY" + i + " is at: " + foodList.get(i).getY());
    }

    //PheromoneList:
    pheroList = F1.getPheroList();

    for (int i = 0; i < foodList.size(); i++) {
      int fh = foodList.get( i ).getY();
      int fw = foodList.get( i ).getX();
      System.out.println( "Food Source" + i + " created at: " + fh + "(fh) and " + fw + "(fw)" );
      // System.out.println("Ant" + i + " moved to: " + aw);

      gc.setFill( Color.DARKGREEN );
      gc.fillRect(fw , fh, 10, 10 );   // Draw the food source
    }

    for (int i = 0; i < antList.size(); i++) {
      int ah = antList.get( i ).getY();
      int aw = antList.get( i ).getX();
      System.out.println( "Ant" + i + " created at: " + ah + "(ah) and " + aw + "(aw)" );
      // System.out.println("Ant" + i + " moved to: " + aw);

      gc.setFill( Color.DARKRED );
      gc.fillRect( aw, ah, 10, 10 );   // Draw the ant
    }

    for (int i = 0; i < pheroList.size(); i++) {
      int ph = pheroList.get( i ).getY();
      int pw = pheroList.get( i ).getX();
      System.out.println( "Pheromone" + i + " created at: " + ph + "(ph) and " + pw + "(pw)" );
      // System.out.println("Ant" + i + " moved to: " + aw);

      gc.setFill( Color.BLUEVIOLET );
      gc.fillRect( pw, ph, 10, 10 );   // Draw the pheromone
    }

//
//      if (ah > hillY) {
//        for (int h = hillY; h <= ah - 10; h++) {     //Draw the way of the ant
//          gc.setFill(Color.OLIVEDRAB);
//          gc.fillRect(hillX, h, 10, 10);
//        }
//      } else {
//        for (int h = hillY; h >= ah + 10; h--) {
//          gc.setFill(Color.OLIVEDRAB);
//          gc.fillRect(hillX, h, 10, 10);
//        }
//      }
//      if (aw > hillX) {
//        for (int w = hillX; w <= aw - 10; w++) {
//          gc.setFill(Color.OLIVEDRAB);
//          gc.fillRect(w, ah, 10, 10);
//        }
//      } else {
//        for (int w = hillX; w >= aw + 10; w--) {
//          gc.setFill(Color.OLIVEDRAB);
//          gc.fillRect(w, ah, 10, 10);
//        }
//      }


    for (int i = 0; i < F1.getFoodList().size(); i++) {
      int fh = F1.getFoodList().get( i ).getY();
      int fw = F1.getFoodList().get( i ).getX();
      //System.out.println( "Food Source" + i + " created at: " + fh + "(fh) and " + fw + "(fw)" );
      // System.out.println("Ant" + i + " moved to: " + aw);

      gc.setFill( Color.DARKGREEN );
      gc.fillRect( fw, fh, 10, 10 );

    }

    for (int i = 0; i < F1.getAntList().size(); i++) {
      int fh = F1.getAntList().get( i ).getY();
      int fw = F1.getAntList().get( i ).getX();
      //System.out.println( "Food Source" + i + " created at: " + fh + "(fh) and " + fw + "(fw)" );
      // System.out.println("Ant" + i + " moved to: " + aw);

      gc.setFill( Color.DARKRED );
      gc.fillRect( fw, fh, 10, 10 );

    }

    for (int i = 0; i < F1.getPheroList().size(); i++) {
      int fh = F1.getPheroList().get( i ).getY();
      int fw = F1.getPheroList().get( i ).getX();
      //System.out.println( "Food Source" + i + " created at: " + fh + "(fh) and " + fw + "(fw)" );
      // System.out.println("Ant" + i + " moved to: " + aw);

      gc.setFill( Color.BLUEVIOLET );
      gc.fillRect( fw, fh, 10, 10 );

    }
    gc.setFill( Color.DARKRED );
    gc.fillRect( hillX, hillY, 20, 20 );   // Draw the AntHill




//            FadeTransition ft = new FadeTransition();
//            ft.setDuration( javafx.util.Duration.seconds( 2 ) );
//            ft.setFromValue( 1 );
//            ft.setToValue( 0.1 );
//            ft.play();

//        String imageSource = "https://nz1.ru/uploads/posts/2017-01/1485857832_2.jpg";
//        Image image = new Image( imageSource );
//        ImagePattern imagePattern = new ImagePattern( image );
//        gc.setFill( imagePattern );

    return canvas;
}

  @Override
  public void start(Stage primaryStage) throws Exception {

  }
  public void refresh(){

  }
}
