package sample.service;

import javafx.scene.control.TextField;

public interface FormService {
  boolean validateData(int width, int height, int hillX, int hillY, int foodCount, TextField wf, TextField hf, TextField hfX, TextField hfY, TextField ff);
}
