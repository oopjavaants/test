package sample;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Ant{
  //Ant Attributes:
  Point location;
  boolean hasFood = false;
  int speed = 1;
  int crumbs = 3;
  //test TempPheroList
  List<Pheromone> tmpPheroList = new ArrayList<>();
  int pheroCount = 0;
  int pheroFrequence = 5;

  List<Point> visits = new ArrayList<>();
  int visitDelete = pheroFrequence + 50; //ToDo: better Calculation (MovementSpeed + 2) Idk
  int visitCount = 0;


  //Ant Constructor:
  public Ant(Point startLocation, int speed){
    this.location = startLocation;
    this.speed = speed;
  }
  public Ant(){
    location = new Point();
  }

  //Ant Functions:
  public void setHasFood(){
    if(this.hasFood){
      hasFood = false;
    } else {
      hasFood = true;
    }
  }
  public int getX(){
    return this.location.x;
  }
  public int getY(){
    return this.location.y;
  }
  public void move(int newX, int newY){
    this.location.setXY(newX,newY);
  }
  public void move2(Point target){
    this.location = target;
  }
  @SuppressWarnings("Duplicates")
  public Point smartMove(Point desire){
    //ToBeDone
    Point result = location;
    Point distance = new Point();
    if (desire.x > result.x){
      distance.x = desire.x - result.x;
    }else{
      distance.x = result.x - desire.x;
    }
    if (desire.y > result.y){
      distance.y = desire.y - result.y;
    }else{
      distance.y = result.y - desire.y;
    }
    //Give Distance:
    System.out.println("Distance: "+distance.toString());
    Point tmpMoved = new Point(0,0);
    Point tmpPhero = new Point();
    for(int i=0;i<speed;i++){
      if(distance.x == 0 && distance.y == 0){
        break;
      }
      //Visit remove!
      visitCount = visitCount + 1;
      if(visitCount % visitDelete == 0){
        if(visits.size() != 0){
          visits.remove(0);
          System.out.println("DELETED FIRST VIDSITED");
        }
      }

      if(distance.x > distance.y){
        tmpMoved.x += 1;
        distance.x -= 1;
      }else{
        tmpMoved.y += 1;
        distance.y -= 1;
      }


      if(desire.x < location.x){
        tmpPhero.x = location.x - tmpMoved.x;
      }else{
        tmpPhero.x = location.x + tmpMoved.x;
      }
      if(desire.y < location.y){
        tmpPhero.y = location.y - tmpMoved.y;
      }else{
        tmpPhero.y = location.y + tmpMoved.y;
      }
      //Inserting into TempList to Ant Obj. with given Frequency
      pheroCount++;
      if(pheroCount%pheroFrequence == 0){
        tmpPheroList.add(new Pheromone(tmpPhero));
        //own pheromones are ignored
        visits.add(tmpPhero);
      }
    }

    if(desire.x < location.x){
      result.x -= tmpMoved.x;
    }else{
      result.x += tmpMoved.x;
    }
    if(desire.y < location.y){
      result.y -= tmpMoved.y;
    }else{
      result.y += tmpMoved.y;
    }
    return result;
  }
  public void clearTmpPhero(){
    tmpPheroList.clear();
    //System.out.println("Cleared Temp PheroList");
  }
  //To Be Deleted:
  public void showTmpPheroList(){
    for(int i=0;i<tmpPheroList.size();i++){
      System.out.println("TEMP!!!Pheromone " + i + " is at: " + tmpPheroList.get(i).location.toString());
    }
  }
}
