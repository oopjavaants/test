package sample;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Field {
  //Field Attributes:
  Point a;
  Point d;

  //AntHill as Attribute of Field:
  AntHill antHill;

  //Lists of all 3 Types that have Locations:
  List<Ant> antList;
  List<FoodSource> foodList;
  List<Pheromone> pheroList;

  //Field Constructor:
  public Field(boolean cmd, int xLength, int yLength, int antHillX, int antHillY, int antCount, int antSpeed, int foodCount) {
    this.a = new Point();
    this.d = new Point(xLength, yLength);
    if(cmd){
      System.out.println("FieldSize: " + getArea());
    }
    //Making AntHill
    //antHill = new AntHill(new Point(antHillX, antHillY));
    antHill = new AntHill();
    antHill.setPosition(new Point(antHillX,antHillY));
    if(cmd){
      System.out.println("AntHillPosition: " + antHill.toString());
    }
    //Making Ants
    antList = new ArrayList<>();
    for(int i=0;i<antCount;i++){
      Point tHill = new Point(antHill.getPositionA().x,antHill.getPositionA().y); //Strangest Bug ever!
      antList.add(new Ant(tHill, antSpeed));
      if(cmd){
        System.out.println("Ant " + i + " created at: " + antList.get(i).location.toString());
      }
    }
    //Making FoodSources:
    foodList = new ArrayList<>();
    for(int i=0;i<foodCount;i++){
      FoodSource newFoodSource = new FoodSource(rndLocation());
      boolean alreadyIn = false;
      for(int j=0;j<foodList.size();j++){
        if(newFoodSource.location.x == foodList.get(j).location.x && newFoodSource.location.y == foodList.get(j).location.y){
          foodList.get(j).amount += newFoodSource.amount;
          alreadyIn = true;
          if(cmd) {
            System.out.println("Added " + newFoodSource.amount + " to FoodSource at: " + foodList.get(j).location + " now: " + foodList.get(j).amount);
          }
        }
      }
      if(alreadyIn){
        continue;
      }else{
        foodList.add(newFoodSource);
        if(cmd) {
          System.out.println("Added new Food Source at: " + newFoodSource.location);
        }
      }
    }
    //Making PheromoneList:
    pheroList = new ArrayList<>();
    if(cmd){
      System.out.println("Pheromone List created:");
    }


    if(cmd){
      System.out.println("StartUp finished!\n" +
        "####################################################################");
    }
  }

  public List <Ant> getAntList() {
    return antList;
  }

  public List <FoodSource> getFoodList() {
    return foodList;
  }

  public List <Pheromone> getPheroList() {
    return pheroList;
  }

  //Field Functions:
  public int getArea() {
    return this.d.x * this.d.y;

  }

  public Point rndLocation() {
    Random rnd = new Random();
    return new Point(rnd.nextInt(this.d.x), rnd.nextInt(this.d.y));
  }

  public void addPhero(Ant ant){
    if(ant.tmpPheroList.size() == 0){
      System.out.println("YES");
    }
    for(int i=0;i<ant.tmpPheroList.size();i++){
      pheroList.add(ant.tmpPheroList.get(i));
      System.out.println("Pheromone " + i + " created at: " + pheroList.get(i).location.toString());
    }
    ant.clearTmpPhero();

    //Collision Detection here!
    //Maybe make Pheromone add to each other
  }
  public void printPheromones(){
    for(int i=0;i<pheroList.size();i++){
      System.out.println("Pheromone " + i + " is at: " + pheroList.get(i).location.toString());
    }
  }
  //ToDo; FoodSpawnRate!
  public void foodSpawn(int amount){
    for(int i=0;i<amount;i++){
      FoodSource newFoodSource = new FoodSource(rndLocation());
      boolean alreadyIn = false;
      for(int j=0;j<foodList.size();j++){
        if(newFoodSource.location.x == foodList.get(j).location.x && newFoodSource.location.y == foodList.get(j).location.y){
          foodList.get(j).amount += newFoodSource.amount;
          alreadyIn = true;
          System.out.println("Added " + newFoodSource.amount + " to FoodSource at: " + foodList.get(j).location + " now: " + foodList.get(j).amount);
        }
      }
      if(alreadyIn){
        continue;
      }else{
        foodList.add(newFoodSource);
        System.out.println("Added new Food Source at: " + newFoodSource.location);
      }
    }
  }

  @SuppressWarnings("Duplicates")
  public void movement(){
    for(int i=0;i<antList.size();i++){

      Point tmpDistance = new Point();
      Point distance = new Point();
      Point desire = new Point();



      Point testhill = new Point(5,5);
      if(antList.get(i).hasFood){
        System.out.println("Move to AntHill" + testhill);
        //Move to Anthill
        antList.get(i).move2(antList.get(i).smartMove(testhill));
        //System.out.println("ANTHILL!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!: " + this.antHill.getPositionA());
        //System.out.println("ISt sie da?: " + antList.get(i).location);
        if(antList.get(i).location.x == testhill.x && antList.get(i).location.y == testhill.y){
          antList.get(i).hasFood = false;
          System.out.println("ARived at AntHill @" + antList.get(i).location);
        }
        //Fill Pheromones
        for(int j=0;j<antList.get(i).tmpPheroList.size();j++){
          pheroList.add(antList.get(i).tmpPheroList.get(j));
          System.out.println("New Pheromone at " + antList.get(i).tmpPheroList.get(j).location.toString());
          //ToDo: double Pheromone adding strength?
          for(int k=0;k<pheroList.size();k++){
            if(antList.get(i).tmpPheroList.get(j).location.x == pheroList.get(k).location.x && antList.get(i).tmpPheroList.get(j).location.y == pheroList.get(k).location.y){
              pheroList.get(k).strength = pheroList.get(k).strength + 1;
            }
          }
        }
        //delete temporary Pheromone List
        antList.get(i).clearTmpPhero();


      }else{
        List<Pheromone> tmpPheroList2 = this.pheroList;
        for(int j=0;j<antList.get(i).visits.size();j++) {
          for (int k = 0; k < this.pheroList.size(); k++) {
            if (antList.get(i).visits.get(j).x == this.pheroList.get(k).location.x && antList.get(i).visits.get(j).y == this.pheroList.get(k).location.y) {
              tmpPheroList2.remove(this.pheroList.get(k));
              continue;
            }
          }
        }

        //through tmpPheroList
        for(int j=0;j<tmpPheroList2.size();j++){
          if(tmpPheroList2.get(j).location.x > antList.get(i).location.x){
            tmpDistance.x = tmpPheroList2.get(j).location.x - antList.get(i).location.x;
          }else{
            tmpDistance.x = antList.get(i).location.x - tmpPheroList2.get(j).location.x;
          }
          if (tmpPheroList2.get(j).location.y > antList.get(i).location.y){
            tmpDistance.y = tmpPheroList2.get(j).location.y - antList.get(i).location.y;
          }else{
            tmpDistance.y = antList.get(i).location.y - tmpPheroList2.get(j).location.y;
          }
          if((distance.x + distance.y) <= (tmpDistance.x + tmpDistance.y)){ // <= zu < wenn == BUG: >=
            distance = tmpDistance;
            desire = tmpPheroList2.get(j).location;
          }
        }
        //through Foodlist
        for(int j=0;j<foodList.size();j++){
          if(foodList.get(j).location.x > antList.get(i).location.x){
            tmpDistance.x = foodList.get(j).location.x - antList.get(i).location.x;
          }else{
            tmpDistance.x = antList.get(i).location.x - foodList.get(j).location.x;
          }
          if (foodList.get(j).location.y > antList.get(i).location.y){
            tmpDistance.y = foodList.get(j).location.y - antList.get(i).location.y;
          }else{
            tmpDistance.y = antList.get(i).location.y - foodList.get(j).location.y;
          }
          if((distance.x + distance.y) <= (tmpDistance.x + tmpDistance.y)){
            distance = tmpDistance;
            desire = foodList.get(j).location;
          }
          //if ==
        }
        antList.get(i).move2(antList.get(i).smartMove(desire));
        //checking if on pheromone:
        for(int j=0;j<pheroList.size();j++){
          if(pheroList.get(j).location.x == antList.get(i).location.x && pheroList.get(j).location.y == antList.get(i).location.y){
            antList.get(i).visits.add(antList.get(i).location);
          }
        }
        //checking for food:
        for(int j=0;j<foodList.size();j++){
          if(foodList.get(j).location.x == antList.get(i).location.x && foodList.get(j).location.y == antList.get(i).location.y){
            antList.get(i).hasFood = true;
            if(foodList.get(j).decrement(1)){
              foodList.remove(j);
            }else{

            }
          }
        }
        System.out.println("Ant " + i + " moved to: " + antList.get(i).location.toString());

        //Fill Pheromones
        for(int j=0;j<antList.get(i).tmpPheroList.size();j++){
          pheroList.add(antList.get(i).tmpPheroList.get(j));
          System.out.println("New Pheromone at " + antList.get(i).tmpPheroList.get(j).location.toString());
          //ToDo: double Pheromone adding strength?
          for(int k=0;k<pheroList.size();k++){
            if(antList.get(i).tmpPheroList.get(j).location.x == pheroList.get(k).location.x && antList.get(i).tmpPheroList.get(j).location.y == pheroList.get(k).location.y){
              pheroList.get(k).strength = pheroList.get(k).strength + 1;
            }
          }
        }
        //delete temporary Pheromone List
        antList.get(i).clearTmpPhero();
      }




    }



  }
}
