package sample;

import javafx.animation.Animation;
import javafx.application.Application;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.util.List;

public abstract class AbstractConfig extends Application {

  protected Stage primaryStage;
  protected GridPane grid;
  protected BorderPane rootLayout;
  protected Animation animation;

  protected List<Ant> antList;
  protected List<Pheromone> pheroList;
  protected List<FoodSource> foodList;

  //StartParameters:
  protected boolean cmd = false;    //Testing reasons (true gives CommandLine Prints)
  protected int antCount = 1;     //Starting amount of Ants
  protected int antHillSize = 2;     //AntHill Size in one direction
  protected int antSpeed = 10;       //Ant Speed (Steps per Timer Intervall)

  protected Field F1;
}
