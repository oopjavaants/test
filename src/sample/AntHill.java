package sample;
public class AntHill{
  //AntHill Attributes:
  Point position;

  //AntHill Constructor:
  public AntHill(){
    this.position = new Point(50,50);
  }

  //AntHill Functions:
  public Point getPositionA(){
    return position;
  }
  public void setPosition(Point position){
    this.position = position;
  }
  @Override
  public String toString(){
    return position.toString();
  }
}
